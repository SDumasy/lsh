FROM python:3.7-slim

RUN apt-get update -y \
    && apt-get install -y gnupg curl \
    && apt-get update -y \
    && apt-get install -y \
        python3-dev python3-pip \
    && apt-get install -y build-essential \
    && pip3 install --upgrade pip\
    && mkdir /app

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
WORKDIR /app
ADD . /app/
RUN pip install -e .

EXPOSE 5050
WORKDIR /app

CMD ["uvicorn", "src.lsh.main:app", "--host", "0.0.0.0", "--port", "5050"]
