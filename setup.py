from setuptools import setup, find_packages

setup(
    name="lsh",
    version="0.0.1",
    description="lsh",
    author="Stephan",
    package_dir={"": "src"},
    packages=find_packages('src'),
    zip_safe=False,
    install_requires=open('requirements.txt').read().splitlines(),
)