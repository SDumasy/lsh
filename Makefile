release_major:
	bumpversion "major" setup.py

build:
	docker build -t $$(python setup.py --name):$$(python setup.py --version) .

publish:
	docker tag $$(python setup.py --name):$$(python setup.py --version) xcceleratedbrian/xccl-lsh:$$(python setup.py --version)
	docker push xcceleratedbrian/xccl-lsh:$$(python setup.py --version)
run:
	uvicorn src.lsh.main:app --host 0.0.0.0 --port 5050 --reload

kill:
	kill -9 $$(lsof -t -i:81 -sTCP:LISTEN)

install:
	helm install xccl-lsh deployment/

upgrade:
	helm upgrade xccl-lsh deployment/

uninstall:
	helm uninstall xccl-lsh