
resource "google_compute_network" "vpc" {
 name = "xccl-lsh-network"
 auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnetworks" {
 name = "xccl-lsh-subnetworks"
 ip_cidr_range = "172.30.0.0/22"
 network = google_compute_network.vpc.id
}
//
//resource "google_compute_router" "xccl-lsh-router" {
//  name    = "xccl-lsh-router"
//  region  = google_compute_subnetwork.subnetworks.region
//  network = google_compute_network.vpc.id
//
//  bgp {
//    asn = 64514
//  }
//}
//
//resource "google_compute_router_nat" "nat" {
//  name                               = "xccl-lsh-nat"
//  router                             = google_compute_router.xccl-lsh-router.name
//  region                             = google_compute_router.xccl-lsh-router.region
//  nat_ip_allocate_option             = "AUTO_ONLY"
//  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
//
//  log_config {
//    enable = true
//    filter = "ERRORS_ONLY"
//  }
//}
