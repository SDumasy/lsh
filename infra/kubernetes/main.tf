
resource "google_service_account" "service-account-gke" {
  account_id   = "service-account-gke"
  display_name = "service account for gke"
}

resource "google_project_iam_member" "gke-sa-role" {
  role    = "roles/storage.objectViewer"
  member ="serviceAccount:${google_service_account.service-account-gke.email}"
}

data "google_compute_network" "vpc" {
  name   = "xccl-lsh-network"
}

data "google_compute_subnetwork" "subnetworks" {
  name   = "xccl-lsh-subnetworks"
}

resource "google_container_cluster" "default" {
  name     = "xccl-lsh-gke"
  initial_node_count  = 3
  network = data.google_compute_network.vpc.self_link
  subnetwork = data.google_compute_subnetwork.subnetworks.name
//
//  private_cluster_config {
//    enable_private_nodes    = true
//    enable_private_endpoint = false
//    master_ipv4_cidr_block  = cidrsubnet("172.30.16.0/16", 12, 511)
//  }

  node_config {
    machine_type = "e2-standard-4"
    service_account = google_service_account.service-account-gke.email
  }

  ip_allocation_policy {}
}

data "google_client_config" "default" {
}

provider "kubernetes" {
  load_config_file       = false
  host                   = google_container_cluster.default.endpoint
  cluster_ca_certificate = base64decode(google_container_cluster.default.master_auth[0].cluster_ca_certificate)
  token                  = data.google_client_config.default.access_token
}

resource "kubernetes_namespace" "xccl-lsh" {
  metadata {
    annotations = {
      name = "xccl-lsh"
    }
    name = "xccl-lsh"
  }
}
