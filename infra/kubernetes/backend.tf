terraform {
  backend "gcs" {
    bucket = "xccl-lsh-sandbox"
    prefix = "infrastructure/kubernetes"
  }
}

provider "google" {
  version = "~> 3.35.0"
  region  = var.region
  project = var.project_id
  zone = var.zone
}
