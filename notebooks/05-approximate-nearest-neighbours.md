---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.6.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Finding duplicate questions

You might have heard about the Quora Duplicate Question competition [^1] that was hosted on Kaggle not so long ago. The goal of this competition was to classify whether question pairs are duplicates or not.

For this competition, quora supplied the competitors with 404.290 pairs of questions and a flag whether they should be considered duplicates. 

[^1]: https://www.kaggle.com/c/quora-question-pairs

```python
import pandas as pd

questions = pd.read_csv('data/quora-question-pairs/train.csv')
print(questions.shape)
questions.head()
```

```python
questions.loc[lambda d: d['qid2'] == 10]
```

<!-- #region -->
Most approaches were something in these three categories:

### Traditional
- Get word embedding distances
- calculate tf-idf weighted jaccard index
- Calculate string distance metrics
- Chuck it all in XGBoost
- ...
- profit

### Deep learning
- Siamese attention based neural nets with or without pre-trained word embeddings

### Magic features
Due to the way Quora built up the training dataset there was some data leakage in the graph structure of the questions. These kinds of features helped the most in the end

## Practice
Let's now say you're a Data Scientist at a large Dutch temporary employment agency. This agency has been scraping online vacancies for the last 4 years and have built up a repository containing about 140.000.000 of these vacancies. And let's also say you're asked to deduplicate all these vacancy texts. You hop on google and the first thing you find is this Kaggle competition and you think: what worked for them must work for us as well, right?

### question: What will go wrong?
First of all, you're probably going to find out that there are not 404.290 labelled duplicates. Probably there aren't any labelled examples at all. So you build an google drive spreadsheet and ask some people to help you label. 

After labelling you start modelling and you've seen that the deep learning approach won the competition, so you're going to try that one first. After quite some struggles getting GPU instances it turns out that this approach performs rather terribly, probably due to a lack of data.

That's when you decide to move to the traditional approach and that seems to work rather well. You model for a while and then try to put your solution into production. You generate the testing dataset and that's when it hits you. Your model does pairwise comparisons of individual question pairs. This means you have to do $140.000.000^2$ comparisons. You realize the entire approach is flawed but you were planning to leave anyway so you durdle along a bit and leave shortly after.

### What you should have done


1. Don’t just start with deep learning. 

You can go far with a simple model and good data. Only move towards more complex models once you’ve exhausted easier methods.

2. Think infrastructure first, model second. 

How is the model going to be served?


<!-- #endregion -->

# The problem rephrased

The problem our predecessor tried to solve was:

> Given two vacancies, are they the same

Whereas the problem that actually should have been solved was

> For a certain definition of closeness, efficiently find items in a large corpus which are similar

Essentially we're searching for nearest neighbours.

Because our prediction data doesn't look like this

```python
questions.head()
```

but like this


```python
all_questions = (
    pd.DataFrame(pd.concat([questions['question1'], questions['question2']]).unique())
    .assign(question=lambda d: d[0].apply(str))
    .drop(columns=[0])
    .dropna()
    .loc[lambda d: d['question'].str.len() > 10]
)

all_questions.head()
```

<!-- #region -->
This means we need two things:

1. A way to encode our vacancies into a meaningful vector representation
2. A way to efficiently search for nearest neighbours in this vector space


It turns out there are some nice algorithms for this efficient search as long as we can express our documents as vectors where similar vectors correspond to similar documents

### Encoding


#### Shingling
The simplest way to represent documents as sets, for the purpose of identifying lexically similar documents is to encode our document as a set of ngrams that appear in it. If we do so, then documents that sharepieces as short as sentences or even phrases will have many common elements in their sets, even if those sentences appear in different orders in the two documents. This process is called shingling.

A k-shingle is a substring of length k found in a document. We can pick k to be any constant we like. However, if we pick k too small, then we would expect most sequences of k characters to appear in most documents. A rule of thumb would be to pick k to be large enough that the probability of any given shingle appearing in any given document is low.

Let's say we pick k=4] and frequently observe about 20 characters (alphabet without uncommon letters). This would leave about $20^4$ possible shingles (160.000). As our questions are typically much smaller than that we should be fine with this choice of shingle

<!-- #endregion -->

```python
%matplotlib inline
import matplotlib.pyplot as plt
all_questions['question'].apply(len).hist(bins=30)
ax = plt.gca()
ax.set_title('histogram of question length');
```

```python
from sklearn.feature_extraction.text import CountVectorizer

vectorizer = CountVectorizer(
    analyzer='char', 
    ngram_range=(4, 4), 
    strip_accents=None
)

shingles = vectorizer.fit_transform(all_questions['question'])

shingles
```

We can then get all the ids of shingles that were present in the document as follows:

```python
_, doc_shingles = shingles.getrow(0).nonzero()
feature_names = vectorizer.get_feature_names()
print(doc_shingles)
print([feature_names[shi] for shi in doc_shingles])
```

On these shingled documents we can then compute pairwise similarity by taking their jaccard index

$$ sim(s1, s2) = \frac{\text{shingles}(s_1) \cap \text{shingles}(s_2)}{\text{shingles}(s_1) \cup \text{shingles}(s_2)} $$

The vector space we end up in is quite large due to its immense sparsity. It turns out that these sparse systems are tricky to deal with so we want to compress the shingles into document signatures. The main goal for a signature is that we can compare the signatures of two documents and estimate their jaccard similarity from the signatures alone. We'll lose a bit of accuracy here but we can gain a large amount of compression.

#### MinHash Signatures
The MinHash algorithm will provide us with a fast approximation to the Jaccard Similarity between two sets.

For each set in our data, we are going to calculate a MinHash signature. The MinHash signatures will all have a fixed length, independent of the size of the set. And the signatures will be relatively short—in the example code, they are only 10 components long.

To approximate the Jaccard Similarity between two sets, we will take their MinHash signatures, and simply count the number of components which are equal. If you divide this count by the signature length, you have a pretty good approximation to the Jaccard Similarity between those two sets.

We can compare two MinHash signatures in this way much quicker than we can calculate the intersection and union between two large sets. This is partly because the MinHash signatures tend to be much shorter than the number of shingles in the documents, and partly because the comparison operation is simpler.

The idea of the algorithm is to permute the columns (shingles) of our matrix of documents and then for each document retrieve the first shingle which occured in the document. 

Let's consider two documents, A and B

- Document A     (A, <b>B</b>, C, D, <b>E</b>, <b>F</b>)
- Document B      (<b>E</b>, G, H, <b>F</b>, I, <b>B</b>, J)

There are three items in common between the sets (B, E, and F), and 10 unique items between the two sets. Therefore, these sets have a Jaccard similarity of 3/10.

When we permute the columns (shingles) we'll end up with different elements in the first place for both of our documents. 
If we do that, then what are the odds that one of the bold items ends up first in the list? It’s given by the number of common items (3) divided by the total number of items (10), or 3/10, the same as the Jaccard similarity.

Rather than actually permuting the matrix we'll define a new order over the columns and return the first nonzero item in that ordering as the minhash. We'll define 

![images/minhash.png](images/minhash.png)

```python
doc1 = [1, 3, 4]

hash_fs = [
    lambda x: (x + 1) % 3,
    lambda x: ((3 * x) + 1) % 3,
    lambda x: ((6 * x) + 3) % 3
]

hashed_doc = []
for hash_f in hash_fs:
    print([hash_f(x) for x in doc1])
#     hashed_doc.append(min(hash_f(x) for x in doc1))
hashed_doc
```

```python
minhash_size = 2

S1 = 10
S2 = 32
S3 = 00
S4 = 10
```

```python
import sympy
from tqdm import trange, tqdm
import numpy as np

def minhash(shingle_mat, sig_size):
    
    a, b = np.random.randint(0, shingle_mat.shape[1], size=(2, sig_size))
    c = sympy.nextprime(shingle_mat.shape[1])

    out = np.empty((shingle_mat.shape[0], sig_size), dtype=np.int)
    for row_id in trange(shingle_mat.shape[0]):
        _, row_shingles = shingle_mat.getrow(row_id).nonzero()
        row_minhash = row_shingles[np.argmin(np.mod(row_shingles[:, np.newaxis] * a + b, c), axis=0)]
        out[row_id, :] = row_minhash
        
    return out
```

```python
minhashes = minhash(shingles, 5)
```

```python
minhashes
```

<!-- #region -->

### Search
So what are some common, fast techniques for nearest neighbour search. 

#### Kd-tree 
The main one you'll encounter is a data structure called a k-d tree. It works by trying to cut our vector space in halves so that it can efficiently narrow down the search space. The main steps of the algorithm include

1. Pick a random dimension
2. Find the median in that dimension
3. split the data
4. repeat


Due to the curse of dimensionality which leads most searches in high dimensional spaces to end up being needlessly fancy brute searches, k-d trees are not suitable for efficiently finding the nearest neighbour in high-dimensional spaces. As a general rule, if the dimensionality is k, the number of points in the data,  N, should be $ N\gg 2^{k}$. Otherwise, most of the points in the tree will be evaluated and the efficiency is no better than exhaustive search. if a good-enough fast answer is required, approximate nearest-neighbour methods should be used instead.
<!-- #endregion -->

#### Banding technique

Assume we have the following minhash matrix:

```python
np.random.seed(42)
np.set_printoptions(linewidth=110)
minhash_sample = np.round(np.random.randint(0, 5, (400, 25)), 2)
minhash_sample
```

An effective way to approximately find similar items is  to divide the signature matrix into b bands consisting of c columns each. For each band, there is a hash function that takes vectors of r integers (the portion of one column within that band) and hashes them to some large
number of buckets. We can use the same hash function for all the bands, but we use a separate bucket array for each band, so columns with the same vector in different bands will not hash to the same bucket.

This means that any document with the same values in a band will definitely hash to the same bucket. We call each document that hashed to the same bucket in any of their bands a candidate pair

![images/banding.png](images/banding.png)


We could for example take the first 5 columns of the matrix and take their sum as a hash function (quite a terrible one, probably something more sophisticated would be better). What's the problem if we would use this hashing method?


```python
band_1_hashes = np.sum(minhash_sample[:, 0:5], axis=1)
band_1_hashes
```

We would get a lot of false positives. Let's try something better.

The following hash method takes a more sophisticated approach to hashing ordered collections. It's the approach that for example Java takes by default for hashing lists.

```
int hashCode = 1;
Iterator<E> i = iterator();
while (i.hasNext()) {
    E obj = i.next();
    hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode());
}
return hashCode;
```


```python
import functools
from collections import Counter

def hash_array_rows(array):
    return np.apply_along_axis(
        lambda row: functools.reduce(lambda prev_hash, value: prev_hash * 31 + value, row, 1), 
        axis=1, 
        arr=array
    )
                  
band_1_hashes = hash_array_rows(minhash_sample[:, 0:5])
band_1_hashes

from collections import defaultdict
candidates = defaultdict(list)

for idx, hash_ in enumerate(band_1_hashes):
    candidates[hash_].append(idx)
Counter(band_1_hashes).most_common(1)
```

If we then take all documents that hash to bucket 11

```python
candidates = minhash_sample[band_1_hashes == 29612378, :]
candidates
```

we only need to compare these documents within their bucket and calculate their jaccard similarity

```python
np.sum(candidates[0] == candidates[1]) / candidates.shape[1]
```

# Choosing our parameters

Suppose we use $b$ bands of $c$ columns each, and suppose that a particular pair of documents have Jaccard similarity $s$
We've seen that the probability that our minhash signatures agree for any particular column of the matrix is also $s$. We can then calculate the probability that these documents become a candidate pair as follows:

1. The probability that the signatures agree in all columns of one particular band is $s^c$
2. The probability that the signatures disagree in at least one column of a particular band is $1 − s^c$
3. The probability that the signatures disagree in at least one column of each of the bands is $(1 − s^c)^b$
4. The probability that the signatures agree in all the columns of at least one band, and therefore become a candidate pair, is $1 − (1 − s^c)^b$

Regardless of the choice of $b$ and $c$ this function has the shape of an s-curve


```python

f = lambda s, c, b: 1 - (1 - s ** c) ** b

ss = np.linspace(0, 1, 100)

fprs = []
tprs = []

minhash_size = 50000
for b in range(1, minhash_size, 1):
    c = minhash_size // b
    probs = f(ss, c, b)
    fprs.append(
        ((probs > 0.5) & (ss < 0.5)).sum() / (ss < 0.5).sum()
    )
    tprs.append(
        ((probs > 0.5) & (ss > 0.5)).sum() / (ss > 0.5).sum()
    )
    
plt.plot(fprs, tprs)
    
    
```

```python
import matplotlib.pyplot as plt
%matplotlib inline

f = lambda s, c, b: 1 - (1 - s ** c) ** b

ss = np.linspace(0, 1, 100)

for c, b in [(5, 5), (3, 7), (10, 500), (5, 1000), (20, 250)]:
    plt.plot(ss, f(ss, c, b), label=f'c={c}, b={b}')
    
plt.xlabel('jaccard similarity of documents')
plt.ylabel('probability of becoming a candidate')
plt.legend();
```

#### Word embeddings

Another way in which we could encode our documents could be to simply take the average word embeddings for each token in the question

```python
# !python -m spacy download en_core_web_md
import numpy as np
import spacy
from tqdm import tqdm
nlp = spacy.load('en_core_web_md') 

# gotta love spacy
with nlp.disable_pipes("tagger", "parser", "ner"):
    doc_vectors = np.array([doc.vector for doc in tqdm(nlp.pipe(all_questions['question']), total=len(all_questions))])
    
doc_vectors
```

```python
doc_vectors[0]
```

# Question: Will the same technique work on our document vectors?


No it won't. In theory we could make bands out of the document vectors but the chance that they will agree exactly in one of their columns is next to zero. Applying minhash to the document vectors also doesn't make sense because the values are continuous rather than discrete. We need something similar to minhashing but rather than approximating the jaccard similarity it should approximate the cosine distance of the vectors.

It turns out we can project our matrix onto another using a random projection matrix R. 
 
![images/randomprojection.png](images/randomprojection.png)

The dot product of our observation vector with the random vector is positive for observation 1 and negative for observation 2. We can round these to 0 and 1

```python
doc_vectors.shape
```

```python
np.random.seed(42)

random_vectors = np.random.randn(300, 1000)


%timeit ((doc_vectors @ random_vectors) > 0).astype(int)

```

```python
np.random.seed(42)

random_vectors = np.random.randn(1000, 300)


%timeit ((doc_vectors @ random_vectors) > 0).astype(int)
```

```python
arr = np.random.randn(10, 5)
arr
```

```python
arr = np.array([[1, 2,3 ], [4, 5, 6]], order='F')
arr.strides
```

```python
import numba
```

```python
@numba.jit()
def compute(doc_vectors, random_vectors):
    return ((doc_vectors @ random_vectors) > 0).astype(np.int64)
```

```python
random_vectors = np.random.randn(300, 1000).astype(np.float32)
```

```python

%timeit compute(doc_vectors, random_vectors)
```

```python
arr.strides
```

```python
arr.T.strides
```

```python

```

```python
projections
```

```python

```

```python

```

```python

```

```python

```

In this case we have bits in all of the cells so we could simply take the `packbits` representation as a hash


```python
from collections import defaultdict
from tqdm import trange

candidates = defaultdict(list)
for i in trange(len(projections)):
    candidates[tuple(projections[i, 0:32])].append(i)
```

```python
first_bucket_candidates = candidates[list(candidates.keys())[0]]

import itertools as it

duplicates = [] 
for l_idx, r_idx in tqdm(it.combinations(first_bucket_candidates, 2), total=len(first_bucket_candidates)**2 / 2):
    
    l = projections[l_idx]
    r = projections[r_idx]
    similarity = np.sum(l == r) / len(l)
    if similarity > 0.9:
        duplicates.append({'l': all_questions.iloc[l_idx]['question'], 'r': all_questions.iloc[r_idx]['question'], 'sim': similarity})
        
result = pd.DataFrame(duplicates)
```

```python
questions.head()
```

```python
result
```

```python
result.merge(questions, left_on=['l', 'r'], right_on=['question1', 'question2'])
```

```python
_22.iloc[2]['r']
```

```python
_22.iloc[2]['l']
```

# Exercise:

Come up with some other use cases for approximate nearest neighbour algorithms



```python
https://towardsdatascience.com/locality-sensitive-hashing-for-music-search-f2f1940ace23
Datasketch
Annoy
```
