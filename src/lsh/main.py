import pickle as p

import pandas as pd
from fastapi import FastAPI

app = FastAPI()


def replace_underscores(question: str):
    return question.replace("_", "")


def do_lsh_magic(question1: str, question2: str):
    similarity = 0
    return similarity


@app.get("/comparetwoq/{question1}/{question2}")
def get_download_usage_csv(question1: str, question2: str):

    return do_lsh_magic(replace_underscores(question1), replace_underscores(question2))


@app.get("/comparetwoId/{question_id_1}/{question_id_2}")
def get_download_usage_csv(question_id_1: str, question_id_2: str):

    return do_lsh_magic(
        replace_underscores(question_id_1), replace_underscores(question_id_2)
    )


@app.get("/getopksimilar/{question}/{k}")
def find_top_k_similar(question: str, k: int):
    dic = {"question": [replace_underscores(question)]}
    df2 = pd.DataFrame(dic)
    vectorizer = p.load(open("pickledcv", "rb"))
    shingle = vectorizer.transform(df2["question"])

    with open("pickledlsh", "rb") as f:
        top_similar = p.load(f).query(shingle, k, 10)["id"].to_list()
        questions = list(
            pd.read_pickle("all_questions").loc[top_similar]["question"].values
        )
        return questions


@app.get("/")
def hello_world():
    return {"Hello": "World"}
