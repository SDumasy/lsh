import pandas as pd
from pandas import DataFrame
from lsh import lsh
from sklearn.feature_extraction.text import CountVectorizer
import pickle as p 

def get_unique_question_list(csv_path: str):
    """This method geta a list of all questions

    Args:
        csv_path (str): The path of the train csv file

    Returns:
        DataFrame: return a dataframe with two columns (id, questions)
    """
    data = pd.read_csv(csv_path)
    return (pd.concat([data[['qid1', 'question1']].rename(columns={'qid1': 'qid', 'question1': 'question'}),
                            data[['qid2', 'question2']].rename(columns={'qid2': 'qid', 'question2': 'question'})])
                 .drop_duplicates()    
    .dropna()
    .loc[lambda d: d['question'].str.len() > 10]
)


def get_cv(all_questions: DataFrame):
    vectorizer = CountVectorizer(
    analyzer='char', 
    ngram_range=(5, 5), 
    strip_accents=None)
    return vectorizer.fit(all_questions['question'])



if __name__ == "__main__":
    """Run this to pikcle all questions dataframe and the LSH object
    """
    all_questions = get_unique_question_list('../data/train.csv').reset_index()
    shingles = make_shingles(all_questions)
    lsh_model = lsh.LSH(shingles)
    num_of_random_vectors = 15
    lsh_model.train(num_of_random_vectors)

    with open('pickledlsh', 'wb') as f:
        p.dump(lsh_model, f)